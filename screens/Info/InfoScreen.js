import React from 'react';
import { StyleSheet, Image, TouchableHighlight, StatusBar, Platform, AsyncStorage, ImageBackground } from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab,
        Body, Footer, Accordion, View, Text, Title, ListItem, List, Spinner } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Expo from "expo";

const dataArray = [
  { title: "Предыдущие рекомендации", content:  "Прием (осмотр, консультация) врача терапевта" },
  //{ title: "Прошедшие", content:  "Прием (осмотр, консультация) врача терапевта" }
];

class InfoScreen extends React.Component {

  state = {
   loading: true,
   loadingList: true,
   token: '',
   user: {},
   list: []
  }

  static navigationOptions = {
   drawerIcon: ({tintColor}) => (
      <Icon name='alert' style={{color: tintColor}}/>
   )
  }

  _getUserData = async () => {
    try {
      const value = await AsyncStorage.getItem('user_data');

      if (value !== null) {
        // We have data!!
        console.log(" We have data!! _getUserData= " + value);
        let res = JSON.parse(value);
        this.setState({user: res});
      }
     } catch (error) {
       console.log("error" + error);
       // Error retrieving data
     }
  }


  _getToken = async () => {
   try {
     const value = await AsyncStorage.getItem('token');

     if (value !== null) {
       let token = value.replace(/['"«»]/g, '');
       this.setState({token: token});
       this._getRecommendations();
     }
    } catch (error) {
      console.log("error" + error);
    }
  }

  _getRecommendations = async () => {
    try {
      //let API_URL = `https://big-holding.kz/test/redirect.php?url=getRecommendations&h=ast2`;
      let API_URL = `http://10.10.70.26:8888/backend/getRecommendations?h=ast2`;

      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'token': this.state.token,
        },
      });
      let responseJson = await response.json();
      if(responseJson !== null) {
        var data = responseJson;
        console.log(JSON.stringify(data))
        if(data.success) {
          this.setState({list: data.recoms});
        } else {
          console.log('Ошибка! Попробуйте еще раз')
        }
      }
    } catch (error) {
      console.log('Error when call API (_getRecommendations): ' + error.message);
    }
    this.setState({loadingList: false});
  }


  onPressList = (data) => {
    this.props.navigation.navigate('InfoDetails',
        {
          data: data
        });
  }

  _renderHeader(title, expanded) {
   return (
     <View
       style={{flex:1, flexDirection: "row", padding: 10,
       backgroundColor: "#fafafa" }}
     >
       {expanded
         ? <Icon style={{ fontSize: 20, paddingVertical: 10 }} name="arrow-dropdown-circle" />
         : <Icon style={{ fontSize: 20, paddingVertical: 10 }} name="arrow-dropright-circle" />}

       <Text style={{paddingLeft: 3, fontWeight: "400", color: '#212529', paddingVertical: 10 }}>
         {" "}{title}
       </Text>
     </View>
   );
  }


  _renderContent = (content) => {
   return (
     <List>
       {this.state.list.map((data, i) => (
         <ListItem avatar key={i} onPress={() => this.onPressList(data)}>
           <Left>
             <Icon name='information-circle' color='#5b7ea4' style={{fontSize: 20, paddingVertical: 5}}/>
           </Left>
           <Body>
             <Text style={{fontSize: 12, paddingVertical: 5}}>{data.recom_name}</Text>
             <Text style={{fontSize: 12}} note>{data.recom_date}</Text>
           </Body>
         </ListItem>
       ))}
     </List>

   );
  }


  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }

    return (
      <Container>
        <ImageBackground source={require('../../assets/design/just-bgr.png')} style={{width: '100%', height: '100%'}}>
          <Header style={styles.headerTop}>
            <Left>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name='md-menu' />
              </Button>
            </Left>
            <Body style={{flex:1, justifyContent: 'center'}}>
              <Title style={{textAlign: 'left'}}>Информация </Title>
            </Body>
            <Right />
          </Header>

          <Content padder>
            {this.state.loadingList ? <Spinner color='red' /> :
              <View>
                <ListItem>
                  <Icon name='person' color='#5b7ea4' style={{fontSize: 40, paddingVertical: 5}}/>
                  <Body style={{paddingLeft: 10}}>
                    <Text style={{fontSize: 20, paddingVertical: 5}}>{this.state.user.fname} {this.state.user.sname}</Text>
                    <Text style={{fontSize: 12}} note>{this.state.user.bday}</Text>
                    <Text style={{fontSize: 12}} note>{this.state.user.iin}</Text>
                  </Body>
                </ListItem>
                <Accordion
                  dataArray={dataArray}
                  renderHeader={this._renderHeader}
                  renderContent={this._renderContent}
                  expanded={0}
                />
            </View>
          }
          </Content>
          <Footer style={{backgroundColor: '#5b7ea4'}}>
            <FooterTab style={{backgroundColor: '#5b7ea4'}}>

            </FooterTab>
          </Footer>
        </ImageBackground>
      </Container>
    );
  }

  componentDidMount() {
    this._getUserData();
    this._getToken();
  }


  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({loading: false});

  }
}

export default InfoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90,
  },
  rowBottom: {
    zIndex: 1,
    marginTop: -40,
  }
});

// http://bmcudp.kz/kz/
