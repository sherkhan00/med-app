import React from 'react';
import { StyleSheet, Image, TouchableHighlight, StatusBar, Platform, View, ScrollView, AsyncStorage, ImageBackground } from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab,
        Body, Footer, Accordion, Text, Title, ListItem, List, Spinner } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Expo from "expo";


class InfoDetails extends React.Component {


  state = {
   loading: true,
   props_data: {},
   recom_text: '',
   token: ''
  }

  _getToken = async () => {
   try {
     const value = await AsyncStorage.getItem('token');

     if (value !== null) {
       let token = value.replace(/['"«»]/g, '');
       this.setState({token: token});
       this._getRecommendationText();
     }
    } catch (error) {
      console.log("error " + error);
    }
  }


  _getRecommendationText = async () => {
    //console.log(this.state.token)
    try {
      //let API_URL = `https://big-holding.kz/test/redirect.php?url=getRecommendationText&h=ast2&rid=${this.state.props_data.recom_id}`;
      let API_URL = `http://10.10.70.26:8888/backend/getRecommendationText?h=ast2&rid=${this.state.props_data.recom_id}`;

      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'token': this.state.token,
          //Authorization: 'tjNwtrTUtQ',
        },
      });
      let responseJson = await response.json();
      if(responseJson !== null) {
        var data = responseJson;
        console.log(JSON.stringify(data))
        if(data.success) {
          this.setState({recom_text: data.recom_text, loading: false});
        } else {
          this.setState({loading: false});
          console.log('Ошибка! Попробуйте еще раз')
        }
      }
    } catch (error) {
      console.log('Error when call API (_getRecommendationText): ' + error.message);
    }
    this.setState({loading: false});
  }

  _getOnlyText (html){
    let regex = /(<([^>]+)>)/ig;
    let result = html.replace(regex, '');
    return result.replace(/[&]nbsp[;]/gi," ");

  }
  render() {
    return (
      <Container>
        <ImageBackground source={require('../../assets/design/info.png')} style={{width: '100%', height: '100%'}}>
          <Header style={styles.headerTop}>
            <Left>
              <Button transparent onPress={() => this.props.navigation.navigate('InfoScreen')}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body  style={{flex:1, justifyContent: 'center'}}>
              <Title style={{paddingLeft: 2, width: 300, textAlign: 'left'}}>Информация</Title>
            </Body>
            <Right />
          </Header>

          <Content padder>
            {this.state.loading ? <Spinner color='red' /> :
              <ScrollView>
                <View>
                  <Text style={{fontSize: 20, paddingVertical: 10}}>{this.state.props_data.recom_name}</Text>
                  <Text style={{fontSize: 12}} note>{this.state.props_data.recom_date}</Text>
                </View>
                <View style={{marginTop: 20}}>
                  <Text style={{fontSize: 14, fontWeight: 'bold', marginTop: 10}}>Рекомендации</Text>
                  <Text style={{fontSize: 12}}>{this._getOnlyText(this.state.recom_text)}</Text>
                </View>
              </ScrollView>
            }
          </Content>
          <Footer style={{backgroundColor: '#5b7ea4'}}>
            <FooterTab style={{backgroundColor: '#5b7ea4'}}>

            </FooterTab>
          </Footer>
        </ImageBackground>
      </Container>
    );
  }


  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this._getToken();
    let { navigation } = this.props;
    let props_data = navigation.getParam('data', 0);

    this.setState({ props_data: props_data});
  }
}

export default InfoDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90,
  },
  rowBottom: {
    zIndex: 1,
    marginTop: -40,
  }
});

// http://bmcudp.kz/kz/
