import React from 'react';
import { StyleSheet, Image, TouchableHighlight, StatusBar, Platform, WebView, AsyncStorage} from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab,
        Body, Footer, Accordion, View, Text, Title, ListItem, List, Spinner } from 'native-base';
import Expo, {WebBrowser} from "expo";

class ResultsShow extends React.Component {
  constructor( props ) {
      super( props );
      this.webView = null;
  }

  state = {
    loading: false,
    props_data: {},
    token: '',
    list: {},
    loadingFont: true
  }

  _getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('token');

      if (value !== null) {
        let token = value.replace(/['"«»]/g, '');
        console.log("ResultsShow token: " + token)
        this.setState({token: token});
        this._getTableData();
      }
   } catch (error) {
     console.log("error get Token" + error);
   }
  }

  _getTableData = async () => {
    let rid = this.state.props_data.reg_id;
    let tid = this.state.props_data.take_id;

    try {
      let API_URL = "http://10.10.70.26:8888/backend/getAnalisesRes?h=ast2&rid=" + rid + "&tid=" + tid;
      console.log('_getTableData  ' + this.state.token);
      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'token': this.state.token,
        },
      });

      let responseJson = await response.json();
      if(responseJson !== null) {
        var data = responseJson;
        console.log("_getTableData fetch " + JSON.stringify(data));
        if(data.success) {
          //this.setState({ loading: false});

          this.setState({loadingFont: false, list: data});
        } else {
          console.log('Ошибка (_getTableData)!  Попробуйте еще раз!');
        }
      }
    } catch (error) {
      console.log('Error when call API _getAnalyzes: ' + error.message);
    }
    setTimeout(() => { this.sendPostMessage() }, 5000)
  }


  pressDownloadPDF = () => {
    let rid = this.state.props_data.reg_id;
    let tid = this.state.props_data.take_id;
    let token = this.state.token;
    let url = `http://10.10.12.4/ru/cabinet/getlabpdf.php?rid=${rid}&tid=${tid}&token=${token}`
    WebBrowser.openBrowserAsync(url);
  };

  sendPostMessage =  () => {
      console.log( "Sending post message 1"  );
      let data = this.state.list;
      console.log( JSON.stringify(data) );
      this.webView.postMessage( JSON.stringify(data));

  }

  render() {
    console.log("render()" + this.state.list === null + " " + this.webView === null);
    if (this.state.loadingFont) {
      return <Expo.AppLoading />;
    }

    return (
      <Container style={{backgroundColor: '#fff'}}>
        <Header style={styles.headerTop}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('Results')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex:1, justifyContent: 'center'}}>
            <Title style={{textAlign: 'left'}}>Результаты</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
              <Button iconLeft style={{padding: 10, backgroundColor: 'blue', marginTop: 10}}
                  onPress={() => this.pressDownloadPDF()}>
                    <Icon name='cloud-download' style={{color: 'white'}}/>
                    <Text style={{color: 'white'}}>Скачать PDF</Text>
              </Button>
             <WebView
               source={require('../../assets/analise.html')}
               javaScriptEnabled={true}
               domStorageEnabled={true}
               ref={( webView ) => this.webView = webView}
               style={styles.webview}
              />
        </Content>
        <Footer style={{backgroundColor: '#5b7ea4'}}>
          <FooterTab style={{backgroundColor: '#5b7ea4'}}>
          </FooterTab>
        </Footer>
      </Container>
    );
  }

  async componentWillMount() {
    console.log("componentWillMount");
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    let { navigation } = this.props;
    let props_data = navigation.getParam('data', 0);
    this.setState({ props_data: props_data});
    this._getToken();
  }
}

export default ResultsShow;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90,
  },
  rowBottom: {
    zIndex: 1,
    marginTop: -40,
  },
  webview: {
    width: '100%',
    height: 700,
    margin: 0,
    padding: 0,
    flex: 1
  }
});

// http://bmcudp.kz/kz/
