import React from 'react';
import { StyleSheet, Image, TouchableHighlight, StatusBar, Platform, AsyncStorage, Linking, ImageBackground } from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab,
        Body, Footer, Accordion, View, Text, Title, ListItem, List, Spinner } from 'native-base';
import Expo, {WebBrowser, FileSystem} from "expo";

class ResultsScreen extends React.Component {
    state = {
      loading: true,
      list: [],
      user: {},
      login: {},
      token: '',
      loadingFont: true
    }

   static navigationOptions = {
     drawerIcon: ({tintColor}) => (
        <Icon name='paper' style={{color: tintColor}}/>
     )
   }

   _getUserData = async () => {
    try {
      const value = await AsyncStorage.getItem('user_data');

      if (value !== null) {
        let res = JSON.parse(value);
        this.setState({user: res});
      }
     } catch (error) {
         console.log("error" + error);
     }
   }

   _getToken = async () => {
     try {
       const value = await AsyncStorage.getItem('token');

       if (value !== null) {
         let token = value.replace(/['"«»]/g, '');
         this.setState({token: token});
         this._getAnalyzes();
       }
    } catch (error) {
      console.log("error get Token" + error);
    }
   }

  _getAnalyzes = async () => {
    //alert('token: ' + this.state.token)
    try {
      //let API_URL = `https://big-holding.kz/test/redirect.php?url=getAnalyzes&h=ast2`;
      let API_URL = 'http://10.10.70.26:8888/backend/getAnalyzes?h=ast2';

      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'token': this.state.token,
        },
      });

      let responseJson = await response.json();
      if(responseJson !== null) {
        var data = responseJson;
        if(data.success) {
          this.setState({list: data.analyzes});
        } else {
          console.log('Ошибка! Попробуйте еще раз!');
        }
      }
    } catch (error) {
      console.log('Error when call API _getAnalyzes: ' + error.message);
    }
    this.setState({loading: false});
  }


   onPressList = (data) => {
     this.props.navigation.navigate('ResultsShow',
         {
           data: data,
         });

   }

   _retrieveData = async () => {
     try {
       const value = await AsyncStorage.getItem('user');

       if (value !== null) {
         // We have data!!
         let user = JSON.parse(value);
         this.setState({login: user});
       }
      } catch (error) {
        console.log("error" + error);
        // Error retrieving data
      }
   }

   openBrowser = (data) => {

     let rid = data.reg_id;
     let tid = data.take_id;
     let USER_LOGIN = this.state.login.login;
     let USER_PASSWORD = this.state.login.password;
     let url = `http://bmcudp_test/bitrix/components/bmcudp/user_analyzes.text/templates/ajax/getlabres_pdf.php?login=yes&` +
     `rid=${rid}&tid=${tid}&AUTH_FORM=Y&TYPE=AUTH&backurl=/bitrix/components/bmcudp/user_analyzes.text/templates/ajax/getlabres_pdf.php?rid=${rid}&tid=${tid}&USER_LOGIN=${USER_LOGIN}&USER_PASSWORD=${USER_PASSWORD}&Login=Войти`;
     
     /*FileSystem.downloadAsync(
          url,
          FileSystem.documentDirectory + 'small.pdf'
        )
          .then(({ uri }) => {
            console.log('Finished downloading to ', uri);
             WebBrowser.openBrowserAsync(uri);
          })
          .catch(error => {
            console.error(error);
          });*/
      Linking.openURL(url);
      //WebBrowser.openBrowserAsync(url);
   };

  render() {
    if (this.state.loadingFont) {
      return <Expo.AppLoading />;
    }

    return (
      <Container>
        <ImageBackground source={require('../../assets/design/results.png')} style={{width: '100%', height: '100%'}}>
          <Header style={styles.headerTop}>
            <Left>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name='md-menu' />
              </Button>
            </Left>
            <Body style={{flex:1, justifyContent: 'center'}}>
              <Title style={{textAlign: 'left'}}>Результаты </Title>
            </Body>
            <Right />
          </Header>

          <Content padder>
            {this.state.loading ? <Spinner color='red' /> :
              <List>
                <ListItem>
                  <Icon name='person' style={{fontSize: 40, paddingVertical: 5}}/>
                  <Body style={{paddingLeft: 10}}>
                    <Text style={{fontSize: 20, paddingVertical: 5}}>{this.state.user.fname} {this.state.user.sname}</Text>
                    <Text style={{fontSize: 12}} note>{this.state.user.bday}</Text>
                    <Text style={{fontSize: 12}} note>{this.state.user.iin}</Text>
                  </Body>
                </ListItem>
                {this.state.list.map((data, i) => (
                  <ListItem avatar key={i} onPress={() => this.onPressList(data)}>
                    <Left>
                      <Icon name='attach' style={{fontSize: 20, paddingVertical: 5}}/>
                    </Left>
                    <Body>
                      <Text style={{fontSize: 12, paddingVertical: 5}}>{data.analises_name}</Text>
                      <Text style={{fontSize: 12}} note>{data.analises_date}</Text>
                    </Body>
                    <Right>
                      <Icon name='cloud-download' style={{fontSize: 20, paddingVertical: 5}}/>
                    </Right>
                  </ListItem>
                ))}
              </List>
            }
          </Content>
          <Footer style={{backgroundColor: '#5b7ea4'}}>
            <FooterTab style={{backgroundColor: '#5b7ea4'}}>

            </FooterTab>
          </Footer>
        </ImageBackground>
      </Container>
    );
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({loadingFont: false});
    this._retrieveData();
    this._getUserData().done();
    this._getToken();

  }
}

export default ResultsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90,
  },
  rowBottom: {
    zIndex: 1,
    marginTop: -40,
  }
});

// http://bmcudp.kz/kz/
