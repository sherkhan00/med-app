import React from 'react';
import {StyleSheet, Text, View, Image, TouchableHighlight, AsyncStorage, ImageBackground} from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab, Footer} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
import LoginScreen from './LoginScreen';


class HomeScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false,
        };
    }

    static navigationOptions = {
        drawerIcon: ({tintColor}) => (
            <Icon name='home' style={{color: tintColor}}/>
        )
    }


    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('user_data');

            if (value !== null) {
                // We have data!!
                console.log(" We have data!! = " + value);
            }
        } catch (error) {
            console.log("error" + error);
            // Error retrieving data
        }
    }

    render() {
        let {isLoggedIn} = this.state;
        this._retrieveData();

        return (
            <Container>
                <ImageBackground source={require('../assets/design/background.png')} style={{width: '100%', height: '100%'}}>
                    <Header style={{elevation:0}} noShadow transparent>
                        <Left style={{flex: 1, flexDirection: 'row'}}>
                            <Icon name='md-menu' style={{color: '#5b7ea4', marginTop: 10, marginLeft: 10}}
                                onPress={() => this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>

                    <Content>
                        <Grid>
                            {/* LOGO */}
                            <Row style={{marginBottom: 40, marginTop: 40, padding: 10}}>
                                <Col style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                    <Image source={{uri: 'https://big-holding.kz/test/logo.png'}}
                                        style={{width: 300, height: 95}}/>
                                </Col>
                            </Row>

                            <Row style={styles.row}>
                                {/* Запись */}
                                <Col style={styles.columnLeft}>
                                    <TouchableHighlight onPress={() => {
                                        this.props.navigation.navigate('Priem')
                                    }}>
                                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                                            <Image style={{width: 150, height: 130}}
                                                source={require('../assets/zapis.png')}/>
                                        </View>
                                    </TouchableHighlight>
                                </Col>
                                {/* Информация */}
                                <Col style={styles.columnRight}>
                                    <TouchableHighlight onPress={() => {
                                        this.props.navigation.navigate('InfoScreen')
                                    }}>
                                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                                            <Image style={{width: 150, height: 130}}
                                                source={require('../assets/info.png')}/>
                                        </View>
                                    </TouchableHighlight>
                                </Col>
                            </Row>
                            <Row style={styles.chat}>
                                {/* Чат */}
                                <Col style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                    <TouchableHighlight onPress={() => {
                                        this.props.navigation.navigate('InfoScreen')
                                    }}>
                                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                                            <Image style={{width: 100, height: 100}}
                                                source={require('../assets/chat.png')}/>
                                        </View>
                                    </TouchableHighlight>
                                </Col>
                            </Row>
                            <Row style={styles.rowBottom}>
                                {/* Контакты */}
                                <Col style={styles.columnLeft}>
                                    <TouchableHighlight onPress={() => {
                                        this.props.navigation.navigate('Contacts')
                                    }}>
                                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                                            <Image style={{width: 150, height: 130}}
                                                source={require('../assets/contact.png')}/>
                                        </View>
                                    </TouchableHighlight>
                                </Col>
                                {/* Результаты */}
                                <Col style={styles.columnRight}>
                                    <TouchableHighlight onPress={() => {
                                        this.props.navigation.navigate('Results')
                                    }}>
                                        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                                            <Image style={{width: 150, height: 130}}
                                                source={require('../assets/result.png')}/>
                                        </View>
                                    </TouchableHighlight>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>
                    <Footer style={{backgroundColor: '#5b7ea4'}}>
                        <FooterTab style={{backgroundColor: '#5b7ea4'}}>

                        </FooterTab>
                    </Footer>
                </ImageBackground>
            </Container>
        );
    }
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    row: {
        zIndex: 1,
    },
    columnLeft: {
        marginRight: 10,
        alignItems: 'flex-end'
    },
    columnRight: {
        marginLeft: 10,
        alignItems: 'flex-start'
    },
    chat: {
        marginTop: -40,
        zIndex: 999,
    },
    rowBottom: {
        zIndex: 1,
        marginTop: -40,
    },

});

// http://bmcudp.kz/kz/
