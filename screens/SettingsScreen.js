import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, ImageBackground } from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab, Footer } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

class SettingsScreen extends React.Component {

   static navigationOptions = {
     drawerIcon: ({tintColor}) => (
        <Icon name='settings' style={{color: tintColor}}/>
     )
   }

  render() {
    return (
      <Container>
        <ImageBackground source={require('../assets/design/background.png')} style={{width: '100%', height: '100%'}}>
          <Header style={{backgroundColor: '#fff', height: 90}}>
            <Left style={{flex: 1, flexDirection: 'row'}}>
              <Icon name='md-menu' style={{color: '#5b7ea4', marginTop: 10, marginLeft: 10}} onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
          </Header>

          <Content padder>
          <Grid>
            <Row style={styles.row}>
              <Text>SettingsScreen</Text>
            </Row>

          </Grid>
          </Content>
          <Footer style={{backgroundColor: '#5b7ea4'}}>
            <FooterTab style={{backgroundColor: '#5b7ea4'}}>

            </FooterTab>
          </Footer>
        </ImageBackground>
      </Container>
    );
  }
}

export default SettingsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
     zIndex: 1,
  },
  columnLeft: {
    marginLeft: 20
  },
  chat: {
    marginLeft: 10,
    marginTop: -40,
    zIndex: 999,
  },
  rowBottom: {
    zIndex: 1,
    marginTop: -40,
  }
});

// http://bmcudp.kz/kz/
