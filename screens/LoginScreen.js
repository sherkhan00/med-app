import React from 'react';
import {StyleSheet, Text, View, Image, TouchableHighlight, TextInput, AsyncStorage, Alert, ImageBackground} from 'react-native';
import {
    Dimensions, Container, Content, Header, Left, Right, Button,
    Icon, FooterTab, Footer, Spinner, Item, Input, Form, Thumbnail, Toast
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
import Expo from "expo";

class LoginScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            isLoggedIn: false,
            message: '',
            token: '',
            loading: false,
            loadingFont: true
        };
        console.log('Dev18')
    }


    onLogin = async () => {
        //this.props.navigation.navigate('HomeNav');
        this.setState({loading: true});
        let {username, password} = this.state;
        //username = '00000005';
        //password = '97755721';
        if (username && password) {
            try {
                //let API_URL = `https://big-holding.kz/test/redirect.php?url=login&login=${username}&password=${password}`;
                let API_URL = `http://10.10.70.26:8888/backend/login`;

                let response = await fetch(API_URL, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: `login=${username}&password=${password}`  //JSON.stringify({'login': username, 'password': password}),
                });

                //console.log(response);
                let responseJson = await response.json();
                if (responseJson !== null) {
                    var data = responseJson;
                    console.log(JSON.stringify(data))
                    let userObj = {login: username, password: password};
                    if (data.success) {
                        this._storeData('user', userObj);
                        this._storeData('token', data.sessionId);
                        this.setState({token: data.sessionId});
                        this._getUserData(data.sessionId);
                    } else {
                        Toast.show({
                            text: 'Ошибка: Неправильный логин или пароль',
                            buttonText: 'Ok',
                            type: "danger",
                            duration: 3000
                        });
                        //Alert.alert('Внимание', 'Ошибка: Неправильный логин или пароль' + username + " " + password)
                        console.log(JSON.stringify(data))
                    }
                }
            } catch (error) {
                console.log('Error when call API (onLogin): ' + error.message);
            }
        } else {
            Toast.show({
                text: 'Заполните логин и пароль',
                buttonText: 'Ok',
                type: "warning",
                duration: 3000
            });
            //Alert.alert('Внимание', 'Заполните логин и пароль ');
        }
        this.setState({loading: false});
    }

    _getUserData = async (token) => {
        try {
            //let API_URL = `https://big-holding.kz/test/redirect.php?url=getUserData&h=ast2`;
            let API_URL = `http://10.10.70.26:8888/backend/getUserData?h=ast2`;

            let response = await fetch(API_URL, {
                method: 'GET',
                headers: {
                    'token': this.state.token,
                },
            });
            let responseJson;
            await response.json().then(function (parsedData) {
                responseJson = parsedData;
            });
            ;
            console.log('_getUserData');
            console.log(responseJson);
            if (responseJson !== null) {
                var data = responseJson;
                if (data) {
                    this._storeData('user_data', data);
                    this.props.navigation.navigate('HomeNav');
                } else {
                    this.setState({loading: false});
                    console.log('Ошибка! Попробуйте еще раз')
                }
            }
        } catch (error) {
            this.setState({loading: false});
            console.log('Error when call API (_getUserData Login): ' + error.message);
        }
    }


    _storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(value));
        } catch (error) {
            console.log("Ошибка сохранении в AsyncStorage! " + error)
        }
    }

    render() {
        if (this.state.loadingFont) {
            return <Expo.AppLoading/>;
        }
        return (
            <Container>
                <ImageBackground source={require('../assets/design/bgr.png')} style={{width: '100%', height: '100%'}}>
                    <Header style={{elevation:0}} noShadow transparent>

                    </Header>
                    <Content contentContainerStyle={{justifyContent: 'flex-start', flex: 1}}>
                        {/* Login Form */}
                        {this.state.loading ? <Spinner color='red'/> :
                            <View>
                                <View style={{alignItems: 'center', marginBottom: 20}}>
                                    <Image source={require('../assets/design/logo.png')}
                                        style={{width: 230, height: 180}}/>
                                </View>
                                <Form style={{marginLeft: 30, marginRight: 30}}>
                                    <Item inlineLabel last>
                                        <Input value={this.state.username}
                                            onChangeText={(username) => this.setState({username})}
                                            placeholder='Логин'
                                        />
                                    </Item>
                                    <Item inlineLabel last>
                                        <Input value={this.state.password}
                                            onChangeText={(password) => this.setState({password})}
                                            placeholder='Пароль'
                                            secureTextEntry={true}
                                        />
                                    </Item>
                                </Form>
                                <Button block style={{margin: 30, backgroundColor: '#5b7ea4'}} onPress={this.onLogin}>
                                    <Text style={{color: 'white'}}>Войти</Text>
                                </Button>
                            </View>
                        }
                        {/*<View>
                    <Image  style={{width: 270, height: 61}} source={require('../assets/logo.png')}/>
                    <TextInput
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}
                        placeholder={'Username'}
                        style={styles.input}
                    />
                    <TextInput
                        value={this.state.password}
                        onChangeText={(password) => this.setState({ password })}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        style={styles.input}
                    />


    contentContainerStyle={{alignItems: 'center', justifyContent: 'center' }}


                    <Button
                        title={'Login'}
                        style={styles.input}
                        onPress={this.onLogin}
                    />
                </View>*/}
                    </Content>
                    <Footer style={{backgroundColor: '#5b7ea4'}}>
                        <FooterTab style={{backgroundColor: '#5b7ea4'}}>
                        </FooterTab>
                    </Footer>
                </ImageBackground>
                
            </Container>
        );
    }


    async componentWillMount() {
        await Expo.Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
        });
        this.setState({loadingFont: false});
    }
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //width: 400,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        height: 40,
        width: 270,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
    },
});

// http://bmcudp.kz/kz/
