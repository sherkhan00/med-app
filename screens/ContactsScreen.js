import React from 'react';
import {StyleSheet, Image, TouchableHighlight, StatusBar, Platform, Linking, ImageBackground} from 'react-native';
import {
    Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab,
    Body, Footer, Accordion, View, Text, Title, ListItem, List
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
import Expo, {WebBrowser} from "expo";

class ContactsScreen extends React.Component {

    state = {
        loading: true,
    }

    static navigationOptions = {
        drawerIcon: ({tintColor}) => (
            <Icon name='call' style={{color: tintColor}}/>
        )
    }

    _handleClickLink = () => {
        WebBrowser.openBrowserAsync('http://bmcudp.kz');
    }

    _handleClickPhone = () => {
        Linking.openURL('tel:+7 (7172) 70-80-90');
    }


    render() {
        if (this.state.loading) {
            return <Expo.AppLoading/>;
        }

        return (
            <Container>
                <ImageBackground source={require('../assets/design/bgr.png')} style={{width: '100%', height: '100%'}}>
                    <Header style={styles.headerTop}>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                                <Icon name='md-menu'/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1, justifyContent: 'center'}}>
                        <Title style={{textAlign: 'left'}}>Контакты </Title>
                        </Body>
                        <Right/>
                    </Header>

                    <Content padder>
                        <ListItem>
                            <Icon name='locate' color='#5b7ea4'
                                style={{fontSize: 20, color: '#5b7ea4', paddingVertical: 5}}/>
                            <Body style={{paddingLeft: 10}}>
                            <Text style={{fontSize: 12}} note>Адрес</Text>
                            <Text style={{fontSize: 14, paddingVertical: 5}}>г. Астана, район Есиль, ул. Е495</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Icon name='call' color='#5b7ea4' style={{fontSize: 20, color: '#5b7ea4', paddingVertical: 5}}/>
                            <Body style={{paddingLeft: 10}}>
                            <Text style={{fontSize: 12}} note>Телефон</Text>
                            <Text style={{fontSize: 14, paddingVertical: 5}} onPress={this._handleClickPhone}>+7 (7172)
                                70-80-90</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Icon name='link' color='#5b7ea4' style={{fontSize: 20, color: '#5b7ea4', paddingVertical: 5}}/>
                            <Body style={{paddingLeft: 10}}>
                            <Text style={{fontSize: 12}} note>Сайт</Text>
                            <Text style={{fontSize: 14, paddingVertical: 5}}
                                onPress={this._handleClickLink}>bmcudp.kz</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Icon name='time' style={{fontSize: 20, color: '#5b7ea4', paddingVertical: 5}}/>
                            <Body style={{paddingLeft: 10}}>
                            <Text style={{fontSize: 12}} note>Время работы</Text>
                            <Text style={{fontSize: 14, paddingVertical: 5}}>пн-сб 8:00 - 20:00</Text>
                            <Text style={{fontSize: 14, paddingVertical: 5}}>вс 9:00 - 18:00</Text>
                            </Body>
                        </ListItem>
                    </Content>
                    <Footer style={{backgroundColor: '#5b7ea4'}}>
                        <FooterTab style={{backgroundColor: '#5b7ea4'}}>

                        </FooterTab>
                    </Footer>
                </ImageBackground>
            </Container>
        );
    }


    async componentWillMount() {
        await Expo.Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
        });
        this.setState({loading: false});
    }
}

export default ContactsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTop: {
        backgroundColor: '#5b7ea4',
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
        height: 90,
    },
    rowBottom: {
        zIndex: 1,
        marginTop: -40,
    }
});
// http://bmcudp.kz/kz/
