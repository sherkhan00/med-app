import React from 'react';
import { StyleSheet, View, Image, TouchableHighlight, StatusBar, Platform, AsyncStorage, ImageBackground } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Right,
  Body,
  FooterTab,
  Footer,
  Spinner
} from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import Expo from "expo";

class ChooseType extends React.Component {

  state = {
    loading: true,
    list: [],
    token: '',
  }

  onGoBack = () => {
    //this.props.navigation.navigate('Priem')
    this.props.navigation.navigate('Home')
  }

  _getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('token');

      if (value !== null) {
        this.setState({token: value});
      }
   } catch (error) {
     console.log("error get Token" + error);
   }
  }



  _getSpecList = async () => {
    //console.log(this.state.token)
    try {
      //let API_URL = `https://big-holding.kz/test/redirect.php?url=getSpecList&h=ast2`;
      let API_URL = `http://10.10.70.26:8888/backend/getSpecList?h=ast2`;

      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'token': this.state.token,
        },
      });
      let responseJson = await response.json();
      //console.log('_getSpecList' + responseJson)
      if(responseJson !== null) {
        var data = responseJson;
        console.log(JSON.stringify(data))
        if(data) {
          this.setState({list: data });
        } else {
          console.log('Ошибка! Попробуйте еще раз')
        }
      }
    } catch (error) {
      console.log('Error when call API _getAnalyzes: ' + error.message);
    }
    this.setState({loading: false});
  }


  onPressList = (id) => {
    let { navigation } = this.props;
    let isOnlyPriem = navigation.getParam('isOnlyPriem', false);

    if(isOnlyPriem) {
      let data = [];
      data['typeId'] = id;
      this.props.navigation.navigate('ChooseTime',
          {
            data: data
          });
    } else {
      this.props.navigation.navigate('ChooseDoctor',
          {
            typeId: id
          });
    }
  }


  render() {
    return (
      <Container >
        <ImageBackground source={require('../../assets/design/choose-cat.png')} style={{width: '100%', height: '100%'}}>
          <Header style={styles.headerTop}>
            <Left>
              <Button transparent onPress={this.onGoBack}>
                <Icon name="arrow-back" color='white'/>
              </Button>
            </Left>
            <Body  style={{flex:1, justifyContent: 'center'}}>
              <Title style={{paddingLeft: 2, width: 300, textAlign: 'left'}}>Выберите категорию</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            {this.state.loading ? <Spinner color='red' /> :
              <List>
                {this.state.list.map((data, i) => (
                  <ListItem avatar key={i} onPress={() => this.onPressList(data.spec_cod)}>
                    <Left>
                      <Icon name='create' color='#5b7ea4' style={{fontSize: 20, paddingVertical: 5}}/>
                    </Left>
                    <Body>
                      <Text style={{paddingVertical: 5}}>{data.spec_name}</Text>
                    </Body>
                  </ListItem>
                ))}
              </List>
            }
        </Content>

          <Footer style={{backgroundColor: '#5b7ea4'}}>
            <FooterTab style={{backgroundColor: '#5b7ea4'}}>

            </FooterTab>
          </Footer>
        </ImageBackground>
      </Container>
    );
  }


  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this._getToken();
    this._getSpecList();

  }
}

export default ChooseType;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90
  }
});

// http://bmcudp.kz/kz/
