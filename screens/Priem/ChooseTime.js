import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, TouchableOpacity, StatusBar, Platform, AsyncStorage, Alert, ImageBackground } from 'react-native';
import {Dimensions, Container, Content, Header, Left, Button, Right, Icon, FooterTab,
        Footer, Body, Title, Item, Input, Form, Picker, Spinner, Toast } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Expo from "expo";

class ChooseTime extends React.Component {

  state = {
    isDateTimePickerVisible: false,
    date: '',
    selectedDoctor: -1,
    shed_id: '',
    doctorName: '',
    doctorId: '',
    spec_cod: '',
    token: '',
    listTime: [],
    loading: false,
    loadingFont: true
  };

  _showDatePicker = () => this.setState({ isDatePickerVisible: true });

  _hideDatePicker = () => this.setState({ isDateTimePickerVisible: false });


  onTimeChange = (value: string) => {
    this.setState({
      shed_id: value
    });
  }

  _getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('token');

      if (value !== null) {
        let token = value.replace(/['"«»]/g, '');
        this.setState({token: token});
      }
   } catch (error) {
     console.log("error get Token" + error);
   }
  }

  _postTalon = async () => {
    let shed_id = this.state.shed_id;

    if(shed_id){
      try {
        //let API_URL = `https://big-holding.kz/test/redirect.php?url=login&login=${username}&password=${password}`;
        let API_URL = `http://10.10.70.26:8888/backend/ambTalonReceptionSave`;

        let response = await fetch(API_URL, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'token': this.state.token,
          },
          body: `h=ast2&i=${shed_id}`  //JSON.stringify({'login': username, 'password': password}),
        });

        //console.log(response);
        let responseJson = await response.json();
        if(responseJson !== null) {
          var data = responseJson;
          if(data.success) {
            Toast.show({
                      text: 'Успешно записались! ' + data.message,
                      buttonText: 'Ok',
                      type: "success",
                      duration: 7000
                    });
            //Alert.alert('Внимание',data.message);
            this.props.navigation.navigate('HomeNav');
            //this.setState({token: data.sessionId});
          } else {
            console.log(JSON.stringify(data))
          }
        }
      } catch (error) {
        console.log('Error when call API (onLogin): ' + error.message);
      }
    } else{
      Toast.show({
                text: 'Выберите время',
                buttonText: 'Ok',
                type: "warning",
                duration: 3000
              });
    }
  }

  _getSheduleDay = async () => {
    let {doctorId, doctorName, spec_cod, cabinet, date} = this.state;
    try {
      //let API_URL = `https://big-holding.kz/test/redirect.php?url=getSheduleDay&h=ast2`;
      let API_URL = `http://10.10.70.26:8888/backend/getSheduleDay?h=ast2&d=-2&doc=${doctorId}&day=${date}&c=${cabinet}`;
      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'token': this.state.token,
        },
      });
      let responseJson = await response.json();
      if(responseJson !== null) {
        var data = responseJson;
        if(data) {
          let res = data.filter(item => item.flag === 0);

          if(res.length !== 0) {
            console.log("_getSheduleDay1 " + JSON.stringify(res));
            this.setState({listTime: res});
          } else {
            console.log("_getSheduleDay 2" + JSON.stringify(res));
            Toast.show({
                      text: 'В данной дате нет свободного времени',
                      buttonText: 'Ok',
                      type: "warning",
                      duration: 3000
                    });
          }


        } else {
          console.log('Ошибка! Попробуйте еще раз')
        }
      }
    } catch (error) {
      console.log('Error when call API _getSheduleDay: ' + error.message);
    }
    this.setState({loading: false});
  }

  onPressBtnSave = () => {
    if(this.state.date){
      this._postTalon();
    } else {
      Toast.show({
                text: 'Выберите дату приема',
                buttonText: 'Ok',
                type: "warning",
                duration: 3000
              });

    }
  }

  _handleDatePicked = (date) => {
    this.setState({
      date: "" + date.toISOString().substring(0, 10),
      isDatePickerVisible: false,
      loading: true
    });
    this._getSheduleDay();
  };


  render() {
    if (this.state.loadingFont) {
      return <Expo.AppLoading />;
    }
    let items = [];
    let dataIncome = this.props.navigation.getParam('data', []);
    let doctorName = dataIncome['doctorName'];
    return (
      <Container>
        <ImageBackground source={require('../../assets/design/priem.png')} style={{width: '100%', height: '100%'}}>
          <Header style={styles.headerTop}>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body  style={{flex:1, justifyContent: 'center'}}>
              <Title style={{paddingLeft: 2, width: 300, textAlign: 'left'}}>Записаться на прием </Title>
            </Body>
            <Right />
          </Header>

          <Content padder>
            {this.state.loading ? <Spinner color='red' /> :
              <Form>
                <Item>
                  <Button iconLeft transparent primary>
                    <Icon name='checkmark-circle' style={{color: '#5b7ea4', fontSize: 20, paddingRight: 10}} />
                    <Text>  {this.state.doctorName}</Text>
                  </Button>
                </Item>
                <Item>
                  <Input value={this.state.date} placeholder='Выберите дату'  />
                  <Icon active name='calendar'  onPress={this._showDatePicker}/>
                </Item>
                <Item style={{paddingVertical: 5}}>
                  <Text style={{fontSize: 14 }}>Выберите время</Text>
                  <Picker
                    note
                    mode="dropdown"
                    style={{ width: 120 }}
                    selectedValue={this.state.shed_id}
                    onValueChange={this.onTimeChange}
                  >
                    {this.state.listTime.map((item, i) => (
                        <Picker.Item key={i} label={item.shedul_time} value={item.shed_id} />
                    ))}
                  {/*
                    <Picker.Item label="10:00" value="1" />
                    <Picker.Item label="10:40" value="2" />
                    <Picker.Item label="11:00" value="3" />
                    <Picker.Item label="11:40" value="4" />
                    <Picker.Item label="12:00" value="5" />
                    */}
                  </Picker>
                </Item>
                {/*<Item style={{paddingVertical: 5}}>
                  <Text style={{fontSize: 14 }}>Выберите врача</Text>
                  <Picker
                    note
                    mode="dropdown"
                    style={{ width: 120 }}
                    selectedValue={this.state.selectedDoctor}
                    onValueChange={this.onDoctorChange}
                  >
                    {items.map((item, i) => (
                        <Picker.Item key={i} label={item.text} value={item.id} />
                    ))}
                  </Picker>
                </Item>*/}
                <Button block style={{backgroundColor: '#5b7ea4'}} onPress={this.onPressBtnSave}>
                  <Text style={{color: 'white'}}>ЗАПИСАТЬСЯ</Text>
                </Button>
              </Form>
            }
            {/*
              <Item>
                <Input value={this.state.time} placeholder='Выберите время'  onPress={this._showTimePicker}/>
                <Icon active name='time'  onPress={this._showTimePicker}/>
              </Item>*/}
              <DateTimePicker
                isVisible={this.state.isDatePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDatePicker}
                mode='date'
              />
              {/*<DateTimePicker
                isVisible={this.state.isTimePickerVisible}
                onConfirm={this._handleTimePicked}
                onCancel={this._hideTimePicker}
                mode='time'
              />*/}
          </Content>
          <Footer style={{backgroundColor: '#5b7ea4'}}>
          <FooterTab style={{backgroundColor: '#5b7ea4'}}>

          </FooterTab>
        </Footer>
        </ImageBackground>
      </Container>
    );
  }

  componentDidMount() {
    let dataIncome = this.props.navigation.getParam('data', []);
    this._getToken().done();
    this.setState({
      doctorName: dataIncome['doctorName'],
      doctorId: dataIncome['doctorId'],
      spec_code: dataIncome['spec_code'],
      cabinet: dataIncome['cabinet'],
    });
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loadingFont: false});
  }
  /*  onDoctorChange = (value: string) => {
      console.log('onDoctorChange: ' + value);
      this.setState({
         selectedDoctor: value
      });
    }*/
}

export default ChooseTime;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90
  },
  rowBottom: {
    zIndex: 1,
    marginTop: -40,
  }
});

// http://bmcudp.kz/kz/
