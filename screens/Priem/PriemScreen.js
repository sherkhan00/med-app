import React from 'react';
import { StyleSheet, View, Image, TouchableHighlight, StatusBar, Platform} from 'react-native';
import {Dimensions, Container, Content, Header, Title, Left, Right, Icon, Button, Text, FooterTab, Footer, Body, Item} from 'native-base';
import {createDrawerNavigator} from 'react-navigation';
import { Col, Row, Grid } from 'react-native-easy-grid';
import ChooseDoctor from './ChooseDoctor';
import ChooseTime from './ChooseTime';
import ChooseType from './ChooseType';
import Expo from "expo";


class PriemScreen extends React.Component {
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });

  }


  constructor(props) {
    super(props);
    this.state = { loading: true };

  }

  static navigationOptions = {
    drawerIcon: ({tintColor}) => (
      <Icon name='create' style={{color: tintColor}}/>
    )
  }

  onGoBack = () => {
    console.log('onGoBack ');
    this.props.navigation.goBack();
  }


  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }

    return (
      <ChooseType navigation={this.props.navigation}/>

    );
    /*return (
      <Container>
        <Header style={styles.headerTop}>
          <Left>
            <Button transparent onPress={this.onGoBack}>
              <Icon name="arrow-back" color='white'/>
            </Button>
          </Left>
          <Body  style={{flex:1, justifyContent: 'center'}}>
            <Title style={{paddingLeft: 2 }}>Выбор записи</Title>
          </Body>
          <Right />
        </Header>
        <Content>
           <View style={{flex: 1, paddingHorizontal: 20, marginTop: 20}}>
            <View style={{flex: 1}}>
              <Text> Выберите удобный для Вас способ записаться на прием</Text>
            </View>
            <View style={{flex: 1, marginTop: 20}}>
              <Item>
                <Button bordered onPress={() => this.props.navigation.navigate('ChooseType', {isOnlyPriem: true}) } style={{flex: 1, paddingVertical: 5}}>
                  <Icon name="calendar" />
                  <Text style={{flex: 1, marginLeft: -5}}>Записаться на прием по времени</Text>
                </Button>
              </Item>
              <Item>
                <Button bordered onPress={() => this.props.navigation.navigate('ChooseType', {isOnlyPriem: false}) } style={{flex: 1, marginTop: 20, paddingVertical: 5}}>
                  <Icon name="person" />
                  <Text style={{flex: 1, marginLeft: -5}}>Записаться на прием к определенному врачу</Text>
                </Button>
              </Item>
            </View>
          </View>
        </Content>
        <Footer style={{backgroundColor: '#5b7ea4'}}>
          <FooterTab style={{backgroundColor: '#5b7ea4'}}>

          </FooterTab>
        </Footer>
      </Container>
    );*/
  }
}

export default PriemScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90
  }
});

// http://bmcudp.kz/kz/
