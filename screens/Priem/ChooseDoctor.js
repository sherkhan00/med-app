import React from 'react';
import { StyleSheet, View, Image, TouchableHighlight, StatusBar, Platform, AsyncStorage, ImageBackground } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Right,
  Body,
  FooterTab,
  Footer,
  Spinner
} from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import Expo from "expo";


class ChooseDoctor extends React.Component {

  state = {
    loading: true,
    list: [],
    token: '',
    spec_code: ''
  }


  _getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('token');

      if (value !== null) {
        this.setState({token: value});
      }
   } catch (error) {
     console.log("error get Token" + error);
   }
  }


  _getShedule = async () => {
    //console.log(this.state.token)
    try {
      //let API_URL = `https://big-holding.kz/test/redirect.php?url=getShedule&h=ast2&spec_cod=983`;
      let API_URL = `http://10.10.70.26:8888/backend/getShedule?h=ast2&d=-2&spec_cod=${this.state.spec_code}`;

      let response = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'token': this.state.token,
        },
      });
      let responseJson = await response.json();
      if(responseJson !== null) {
        var data = responseJson;
        console.log(JSON.stringify(data))
        if(data) {
          this.setState({list: data, loading: false});
        } else {
          console.log('Ошибка! Попробуйте еще раз')
        }
      }
    } catch (error) {
      console.log('Error when call API _getAnalyzes: ' + error.message);
    }
    this.setState({loading: false});
  }


  onGoBack = () => {
    this.props.navigation.goBack();
    /*if(this.state.spec_code)
      this.props.navigation.goBack();
    else
      this.props.navigation.navigate('Priem');*/
  }

  onPressList = (item) => {
    let data = [];
    data['spec_code'] = this.state.spec_code;
    data['doctorId'] = item.doc_id;
    data['doctorName'] = item.spec_name;
    data['cabinet'] = item.cab_num;

    console.log("Pressed ListItem ChooseDoctor " + data['typeId'] + " " + data['doctorId']);
    this.props.navigation.navigate('ChooseTime',
        {
          data: data
        });
  }


  render() {
    let items = this.state.list;

    return (
      <Container >
        <ImageBackground source={require('../../assets/design/choose-spec.png')} style={{width: '100%', height: '100%'}}>
          <Header style={styles.headerTop}>
            <Left>
              <Button transparent onPress={() => this.onGoBack()}>
                <Icon name="arrow-back" color='white'/>
              </Button>
            </Left>
            <Body  style={{flex:1, justifyContent: 'center'}}>
              <Title style={{paddingLeft: 2, width: 300, textAlign: 'left'}}>Выберите специалиста</Title>
            </Body>
            <Right />
          </Header>
          <Content style={{backgroundColor: 'white'}}>
            {this.state.loading ? <Spinner color='red' /> :
              <List>
                {items.map((item, i) => (
                    <ListItem avatar key={i} onPress={() => this.onPressList(item)} style={{paddingVertical: 5}}>
                      <Left>
                        <Thumbnail small source={{uri: 'https://garden.zendesk.com/css-components/avatars/images/jz.png'}} style={{paddingVertical: 5}}/>
                      </Left>
                      <Body>
                        <Text style={{paddingVertical: 5}}>{item.spec_name}</Text>
                      </Body>
                      <Right>
                          <Icon name='create' style={{fontSize: 20, paddingVertical: 5}}/>
                        </Right>
                    </ListItem>
                  ))}
              </List>
            }
        </Content>

          <Footer style={{backgroundColor: '#5b7ea4'}}>
          <FooterTab style={{backgroundColor: '#5b7ea4'}}>

          </FooterTab>
        </Footer>
        </ImageBackground>
      </Container>
    );
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    const spec_code = this.props.navigation.getParam('typeId', -1);
    this.setState({ spec_code: spec_code });
    this._getToken();
    this._getShedule();
  }
}

export default ChooseDoctor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerTop: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    height: 90
  }
});

// http://bmcudp.kz/kz/
