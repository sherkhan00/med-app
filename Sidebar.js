import React from 'react';
import { StyleSheet, View, Image, TouchableHighlight, StatusBar, Platform, SafeAreaView, Text, ScrollView} from 'react-native';
import {createDrawerNavigator, createStackNavigator, createSwitchNavigator, DrawerItems} from 'react-navigation';
import Expo from "expo";
import {Button, Icon} from 'native-base';

class Sidebar extends React.Component {

  render() {
    const { items, ...rest } = this.props;
    const visibleItems = ['Home', 'Priem', 'Results', 'Information', 'Contacts', 'Notification', 'Settings'];
    const filteredItems = items.filter(item => {
      let isVisible = false;
      visibleItems.map(function (eachItem) {
        if(eachItem === item.key)
          isVisible = true;
      });
      return isVisible;
    });
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#353535'}}>
        <View style={styles.menuHeader}>
          <Text style={{padding: 20, fontSize: 18, color:'white'}}>БОЛЬНИЦА УДП</Text>
        </View>
        <ScrollView style={{}}>
          <DrawerItems items={filteredItems} {...rest} />
          <Button iconLeft transparent primary onPress={() => (alert('Logout pressed'))}>
            <Icon name='log-out' style={{color: 'white'}}/>
            <Text style={{paddingLeft: 35, fontSize: 16, fontWeight: 'bold', color:'white'}}>Выход</Text>
          </Button>

        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Sidebar;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuHeader: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
  }
});
