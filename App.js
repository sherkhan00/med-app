import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView, StatusBar, Platform} from 'react-native';
import {createDrawerNavigator, createStackNavigator, createSwitchNavigator, DrawerItems} from 'react-navigation';
import { Root } from "native-base";
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import {Button, Icon} from 'native-base';
import ChooseDoctor from './screens/Priem/ChooseDoctor';
import ChooseType from './screens/Priem/ChooseType';
import ChooseTime from './screens/Priem/ChooseTime';
import ConfirmTime from './screens/Priem/ConfirmTime';
import ContactsScreen from './screens/ContactsScreen';
import PriemScreen from './screens/Priem/PriemScreen';
import InfoScreen from './screens/Info/InfoScreen';
import InfoDetails from './screens/Info/InfoDetails';
import NotifyScreen from './screens/NotifyScreen';
import ResultsScreen from './screens/Analise/ResultsScreen';
import ResultsShow from './screens/Analise/ResultsShow';
import SettingsScreen from './screens/SettingsScreen';

export default class App extends React.Component {
  render() {
    return (
      <Root>
        <AppNavigator />
      </Root>
    );
  }
}


const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
    <View style={styles.menuHeader}>
      <Text style={{padding: 20, fontSize: 18, color:'white'}}>БОЛЬНИЦА УДП</Text>
    </View>
    <ScrollView style={{}}>
      <DrawerItems {...props} style={{}}/>
      <Button iconLeft transparent primary onPress={() => (props.navigation.navigate('SignIn'))}>
        <Icon name='log-out' style={{color: '#353535'}}/>
        <Text style={{paddingLeft: 35, fontSize: 16, fontWeight: 'bold', color:'#353535'}}>Выход</Text>
      </Button>

    </ScrollView>
  </SafeAreaView>
)


const AuthenticationNavigator = createStackNavigator({
  //ResultsShow: ResultsShow,
  SignIn: LoginScreen,
}, {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
});

const PriemNavigator = createStackNavigator({
  ChooseType: ChooseType,
  ChooseDoctor: ChooseDoctor,
  ChooseTime: ChooseTime,
  ConfirmTime: ConfirmTime,
}, {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
});

const ScreensNavigator = createStackNavigator({
  InfoDetails: InfoDetails,
  ResultsShow: ResultsShow,

}, {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
});

const HomeDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Главная`,
    }),
  },
  Priem:  {
    screen: PriemScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Запись на прием`,
    }),
  },
  Results: {
    screen: ResultsScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Результаты анализов`,
    }),
  },
  InfoScreen: {
    screen: InfoScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Информация`,
    }),
  },
  Contacts: {
    screen: ContactsScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Контакты`,
    }),
  },

}, {
  contentComponent: CustomDrawerComponent,
  contentOptions: {
    activeTintColor: '#5b7ea4',
    inactiveTintColor : '#353535',
  }
})

/*Notification: {
  screen: NotifyScreen,
  navigationOptions: ({ navigation }) => ({
    title: `Уведомления`,
  }),
},
Settings: {
  screen: SettingsScreen,
  navigationOptions: ({ navigation }) => ({
    title: `Настройки`,
  }),
},*/


const AppNavigator = createSwitchNavigator({
  Auth: AuthenticationNavigator,
  HomeNav: HomeDrawerNavigator,
  PriemNav: PriemNavigator,
  ScreensNavigator: ScreensNavigator,
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuHeader: {
    backgroundColor: '#5b7ea4',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
  }
});
